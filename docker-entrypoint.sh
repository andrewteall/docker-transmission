#!/bin/sh
set -e
CONFIG=/etc/transmission-daemon/settings.json

if [ -z "$AUTH" ]; then
  echo 'Starting transmission-daemon with no authentication required.'
else
	if [ "$AUTH" = "true" ];then
  	echo 'Starting transmission-daemon with authentication required.'
  	sed -i -e "s/\"rpc-authentication-required\": false,/\"rpc-authentication-required\": true,/" $CONFIG

		if [ -z "$USERNAME" ]; then
		  echo 'Using default username. To change the username set the USERNAME variable when starting the container (-e USERNAME=myuser).'
		else
		  sed -i -e "s/\"rpc-username\": \"transmission\",/\"rpc-username\": \"$USERNAME\",/" $CONFIG
		fi

		if [ -z "$PASSWORD" ]; then
		  echo 'Using default password. To change the password set the PASSWORD variable when starting the container (-e PASSWORD=password).'
		else
		  sed -i -e "s/\"rpc-password\": \"transmission\",/\"rpc-password\": \"$PASSWORD\",/" $CONFIG
		fi
			unset PASSWORD USERNAME
	fi
fi


exec /usr/bin/transmission-daemon --foreground "$@"
