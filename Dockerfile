FROM alpine:latest
LABEL maintainer "andrewteall@gmail.com"

RUN apk add --update \
  transmission-daemon \
  && rm -rf /var/cache/apk/*


RUN mkdir -p /transmission/torrents \
  && mkdir -p /transmission/completed\
  && mkdir -p /transmission/incomplete\
  && mkdir -p /etc/transmission-daemon

COPY settings.json /etc/transmission-daemon/settings.json

VOLUME ["/transmission/completed"]
VOLUME ["/transmission/incomplete"]
VOLUME ["/transmission/torrents"]
VOLUME ["/etc/transmission-daemon"]

EXPOSE 9091 51413/tcp 51413/udp

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["--config-dir", "/etc/transmission-daemon"]
