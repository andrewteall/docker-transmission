# Docker-Transmission

## Description

Docker-Transmission is meant to be a simple and configurable way to run transmission from docker.


## Getting Started
To run the container in it's simplest form run:

`$ docker run  --name=transmission -p 9091:9091 registry.gitlab.com/andrewteall/docker-transmission`

This will start a container listening on localhost with a default configuration with no authentication required to connect to the server.


## Options
These options are the same options from the transmission-daemon manpage. They can be passed in as parameters when running the docker-transmission container. Whenever an argument is passed in the default configuration directory will be overridden and need to be re-specified.

    -a	--allowed x.x.x.x,...
	     Allow RPC access to a comma-delimited whitelist of	IP addresses.
	     Wildcards can be specified	in an address by using '\*'.  Default
	     "127.0.0.1" Example: "127.0.0.\*,192.168.1.\*"

     -b	--blocklist
	     Enable peer blocklists. Transmission understands the bluetack
	     blocklist file format.  New blocklists can	be added by copying
	     them into the config-dir's	"blocklists" subdirectory.

     -c	directory
	     Directory to watch	for new	.torrent files to be added. As they
	     are added to this directory, the daemon will load them into
      Transmission.

    -C	     Do	not watch for new .torrent files.

    -B	--no-blocklist
      Disble blocklists.

    -f	--foreground
      Run in the	foreground and print errors to stderr.

    -g	--config-dir directory
      Where to look for configuration files. This can be	used to	swap
      between using the cli, daemon, gtk, and qt	clients.  See
      http://trac.transmissionbt.com/wiki/ConfigFiles for more informa-
      tion.

    -er --encryption-required
      Encrypt all peer connections.

    -ep --encryption-preferred
      Prefer encrypted peer connections.

    -et --encryption-tolerated
      Prefer unencrypted	peer connections.

    -gsr --global-seedratio ratio
      All torrents, unless overridden by	a per-torrent setting, should
      seed until	a specific ratio

    -GSR --no-global-seedratio
      All torrents, unless overridden by	a per-torrent setting, should
      seed regardless of	ratio

    -h	--help
      Print command-line	option descriptions.

    --incomplete-dir dir
      When adding new torrents, store their contents in directory until
      the torrent is done.

    --no-incomplete-dir
      Don't store incomplete torrents in	a different directory.

    -i	--bind-address-ipv4
      Listen for	IPv4 BitTorrent	connections on a specific address.
      Only one IPv4 listening address is	allowed. Default: 0.0.0.0 (All
      addresses)

    -I	--bind-address-ipv6
      Listen for	IPv6 BitTorrent	connections on a specific address.
      Only one IPv6 listening address is	allowed. Default: :: (All
      addresses)

    -r	--rpc-bind-address
      Listen for	RPC connections	on a specific address. This must be an
      IPv4 address. Only	one RPC	listening address is allowed. Default:
      0.0.0.0 (All addresses)

    --paused
      Pause all torrents	on startup

    -L	--peerlimit-global limit
      Overall peer limit. Useful	on embedded systems where the default
      might be unsuitable. Default: 240

    -l	--peerlimit-torrent limit
      Peer limit	per torrent. Useful on embedded	systems	where the
      default might be unsuitable. Default: 60

    -m	--portmap
      Enable portmapping	via NAT-PMP or UPnP

    -M	--no-portmap
      Disable portmapping

    -o	--dht
      Enable distributed	hash table (DHT).

    -O	--no-dht
      Disable distribued	hash table (DHT).

    -p	--port port
      Port to open and listen for RPC requests on. Default: 9091

    -P, --peerport port
      Port to listen for	incoming peers on. Default: 51413

    -t	--auth
      Require clients to	authenticate themselves.  This doesn't do much
      good unless username and password are also	set.

    -T	--no-auth
      Don't require authentication from clients.

    -u	--username username
      Used for client authentication.

    -v	--password password
      Used for client authentication.

    -V	--version
      Show version number and exit

    --utp   Enable uTP	for peer connections.

    --no-utp
      Disable uTP for peer connections.

    -w	--download-dir
      Where to store downloaded data.

    -e	--logfile
      Where to store transmission's log messages.

    --log-error
      Show error	messages

    --log-info
      Show error	and info messages

    --log-debug
      Show error, info, and debug messages
